
const sinon = require('sinon');
const chai = require("chai");
chai.should();

const Mongo = require("../src/lib/mongo");

describe("Mongo", function () {

  it("Should connect to mongo and select a database", async function () {
    Mongo.should.be.an("object");
    Mongo.should.have.property('records');
  });


  describe('#insert()', () => {
    
    it("Should pass the database result into the callback", async function () {
      let expected = { success: true };
      const insert = sinon.stub(Mongo, "insert");
      insert.yields(null, expected);
      const done = sinon.spy();
      Mongo.insert({ name: "Test Recipe" }, done);
      Mongo.insert.restore();
      sinon.assert.calledWith(done, null, expected);
    });

  });

  describe("#findOne()", () => {
  
    it("Should invoke callback with matching recipe", async function () {
      let expected = require('./stubs/findOne.js');
      const findOne = sinon.stub(Mongo, "findOne");
      findOne.yields(null, expected);
      const done = sinon.spy();
      Mongo.findOne('5f691b9cb6e9664e8878629c', done);
      Mongo.findOne.restore();
      sinon.assert.calledWith(done, null, expected);
    });
    
  });
  
  
});
