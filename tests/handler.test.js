const chai = require('chai');
const expect = chai.expect;
chai.should();

const handler = require('../src/handler');
const error = require("debug")("RecipeAPINodejs::tests:errors");

describe('Handler', () => {
  
  let id = '5f691b9cb6e9664e8878629c';
  let methods = Object.keys(handler).filter((m) => m);
  let expectedMethods = ['find', 'list', 'create', 'remove', 'err'];
  
  it('Should have rest endpoints as properties', () => {
    methods.should.eql(expectedMethods);
    Object.keys(handler).forEach((method) => {
      handler[method].should.be.an('function');
    });
  });
    
  it('Should return all recipes', function () {
    handler
      .list()
      .then(function (res) {
        res.statusCode.should.equal(200);
        res.body.should.be.an("array");
        res.body[0].should.be.an("object");
        res.body[0].should.have.property("name");
        res.name.should.equal("nothing");
      })
      .catch((e) => error);
  });

  it('Should create, find and remove recipes', function () {
    handler
      .create({ body: { name: "Test Recipe" } })
      .then(function (res) {
        res.statusCode.should.equal(200);
        res.should.have.property("body");
      })
      .catch((e) => error);
    handler
      .find({ body: { name: "Test Recipe" } })
      .catch((e) => {
        error(e);
      })
      .then(function (res) {
        res.statusCode.should.equal(200);
        res.body.should.be.an("object");
      })
      .catch((e) => error);
    handler
      .remove({ body: { receip_id: "5f691b9cb6e9664e8878629e" } })
      .then(function (res) {
        res.statusCode.should.equal(200);
        res.should.have.property("body");
      })
      .catch((e) => error);
  });
  
});
