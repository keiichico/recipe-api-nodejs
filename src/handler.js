'use strict';

const Recipes = require('./recipes');
const { jsonify } = require('./lib/util');
const error = require("debug")("RecipeAPINodejs::handler:errors");

module.exports = {

  async find(req) {
    let recipe = await Recipes
      .find(jsonify(req))
      .catch((e) => this.err)
    if (!recipe) {
      return {
        statusCode: 404,
        body: 'Recipe not found'
      }
    }
    return {
      statusCode: 200,
      body: recipe
    };
  },

  async list() {
    return {
      statusCode: 200,
      body: await Recipes.all() || []
    };
  },

  async create(req) {
    let recipe = jsonify(req);
    let {
      insertedId: recipe_id
    } = await Recipes.create(recipe).catch((e) => this.err)
    return {
      statusCode: 200,
      body: recipe_id
    };
  },

  async remove(req) {
    let { recipe_id } = await Recipes.remove(jsonify(req)).catch(
      (e) => this.err
    );
    return {
      statusCode: 200,
      body: `${recipe_id} removed`
    };
  },

  err(e) {
    error({ ...e });
    return {
      statusCode: 406,
      body: JSON.stringify(e.messagee)
    };
  }

};