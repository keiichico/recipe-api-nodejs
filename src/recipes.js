const fs = require('fs')
const Mongo = require("./lib/mongo");

const { error } = require("./lib/util");
var ObjectId = require("mongodb").ObjectID;

class Recipes {
  constructor() {
    Mongo.connect();
  }

  async ids(collection) {
    let data = await this.get();
    return await data.map((d) => d._id);
  }

  async create(recipe) {
    return await Mongo.insert(recipe);
  }

  async update(id, recipe) {
    return await Mongo.replaceOne(ObjectId(id), recipe);
  }

  async remove(query) {
    return await Mongo.remove(query);
  }

  async find(query) {
    return await await Mongo.findOne(query);
  }

  async all() {
    let allRecipes = await this.get();
    return allRecipes.map((doc) => doc) || [];
  }

  async get(query = {}) {
    try {
      return await Mongo.fetch(query);
    } catch (e) {
      console.error(e);
    }
  }
}

module.exports = new Recipes();
