const debug = require("debug");
const log = debug("recipe-api-nodejs:mongo:log");
const error = debug("recipe-api-nodejs:mongo:error");

const MongoClient = require("mongodb").MongoClient;

log.log = console.log.bind(console);
const isArray = (docs) => { Array.isArray(docs) }

class Mongo {

  async connect() {
    let { host, options, database } = this.configureMongoClient();
    try {
      let client = await MongoClient.connect(host, options);
      let db = client.db(database);
      this.records = db.collection('recipes')
      return this;
    } catch (e) {
      error(`ERROR CONNECTING TO MONGO DATABASE`, "\n", e);
      process.exit(0);
    }
  }
  
  async findOne(query) {
    try {
      return await this.records.findOne(query);
    } catch (e) {
      error(e)
    }
  }

  async fetch(query) {
    try {
      return await this.records.find(query).toArray();
    } catch (error) {
      error(e)
    }
  }

  async insert(docs) {
    try {
      return await this.records[isArray(docs) ? "insertMany" : "insertOne"](docs);
    } catch (e) {
      error(e)
    }
  }

  async replaceOne(query, doc) {
    try {
      return await this.records.replaceOne(query, doc);
    } catch (e) {
      error(e)
    }
  }

  async updateMany(query, doc) {
    try {
      return await this.records.updateOne(query, doc);
    } catch (e) {
      error(e)
    }
  }

  async remove(query) {
    try {
      let matches = this.records.find(query);
      let count = await matches.count();
      return await this.records[count > 1 ? "deleteMany" : "deleteOne"](query);
    } catch (e) {
      error(e)
    }
  }

  configureMongoClient() {
    let host =
      process.env.environment === "production"
        ? process.env.MONGODB_PROD_URL
        : (process.env.environment === "stage" ? process.env.MONGODB_PROD_URL : "mongodb://localhost:27017");
    let options =
      process.env.environment === "production"
        ? {
            sslValidate: false,
            useUnifiedTopology: true,
          }
        : {
            useUnifiedTopology: false,
            useNewUrlParser: true,
          };
    return { host, options, database: this.database() };
  }

  database() {
    return process.env.environment === "production" ? "nodebb-prod" : "nodebb";
  }

}

let mongo = new Mongo();
mongo.connect();
module.exports = mongo;
