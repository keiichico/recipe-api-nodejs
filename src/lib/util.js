module.exports = {
  log: (ns) => { return require('debug')(ns || GLOBAL_NAMESPACE) },

  jsonify: (obj) => { return JSON.parse(JSON.stringify(obj)).body }
};
